#!/usr/bin/env python3
import trimesh, argparse, os, math
import numpy as np
import networkx as nx
from texttable import Texttable


def compute_sphericity(S, V):
    return math.pi**(1/3) * (6 * V)**(2/3) / S

def split_mesh(args):

    # Load and clean the mesh if requested
    if args.clean_input_mesh:
        if args.verbose:
            print('Loading and cleaning mesh \'{}\', this may take some time...'.format(args.input_file))
        mesh = trimesh.load(args.input_file, process=True, validate=True)
        mesh.remove_unreferenced_vertices()
        mesh.fix_normals(multibody=True)
    else:
        if args.verbose:
            print('Loading mesh \'{}\'.'.format(args.input_file))
        mesh = trimesh.load(args.input_file)

    vs = float(args.voxel_size)

    # Compute basic statistics on input mesh
    NT = mesh.faces.shape[0]
    BC = mesh.body_count
    WT = mesh.is_watertight
    IV = mesh.is_volume
    S = mesh.area*vs**2
    V = mesh.volume*vs**3
    
    if args.verbose:
        print('Input mesh contains {} triangles forming {} connected components.  '.format(NT, BC))

    # Split mesh using connected faces (faces sharing an edge)
    graph = nx.Graph()
    graph.add_edges_from(mesh.face_adjacency)
    groups = tuple(list(cc) for cc in nx.connected_components(graph))

    # Set up minimum number of triangles considered as a submesh
    # If not used specified, we consider that a submesh contains at least 3 triangles or 1% of base mesh triangles
    min_triangles = max(args.min_triangles, int(NT*args.min_triangles_percentage))

    # Iterate on face subgroups, build and clean submesh and save it
    rows=[('Name','Volume','Surface','Surface/Volume', 'Sphericity', 'Triangles', 'Watertight/Vol')]
    rows.append([args.input_mesh_name,V,S,S/V,compute_sphericity(S,V),NT,'{}/{}'.format(WT,IV)])
    for (i,group) in enumerate(sorted(groups, key=list.__len__, reverse=True)):
        if (i>=args.max_objects) or (len(group) < min_triangles):
            break
        m = mesh.submesh((group,), only_watertight=False)
        assert (m.size == 1), 'Got multiple meshes for one connected face subgroup...'
        m = m[0]
        m.fix_normals(multibody=False)
        m.fill_holes()
        rows.append((args.output_mesh_name.format(i),
            m.volume*vs**3, m.area*vs**2, (m.area*vs**2)/(m.volume*vs**3),
            compute_sphericity(m.area*vs**2, m.volume*vs**3),
            m.faces.shape[0], '{}/{}'.format(m.is_watertight, m.is_volume)))
        m.export(args.output_basename.format(i))
    
    # Print the statistics
    t1 = Texttable()
    t1.add_rows(rows)
    if args.verbose:
        print('\nDetailed statistics')
        print(t1.draw())


if __name__ == '__main__':
    description='Split a STL mesh into face connected submeshes.'
    epilog='Please note that connected meshes cannot be split using this program.'
    parser = argparse.ArgumentParser(prog='split_mesh',
            description=description, epilog=epilog)

    main_args = parser.add_argument_group('Main parameters')
    main_args.add_argument('-i', '--input', type=str, required=True,
            dest='input_file', help='Path to input mesh.')
    main_args.add_argument('-o', '--output', type=str, default=None,
            dest='output_basename', help='Output basename for meshes, default one is computed from input mesh name.')
    main_args.add_argument('-c', '--clean', default=False, action='store_true',
            dest='clean_input_mesh', help='Clean input mesh (may take some time).')
    main_args.add_argument('-nv', '--no-verbose', action='store_false',
            dest='verbose', help='Disable verbosity.')
    main_args.add_argument('-vs', '--voxel-size', type=float,
            dest='voxel_size', help='Voxel size factor.', default=1.)

    mesh_args = parser.add_argument_group('Mesh parameters')
    mesh_args.add_argument('-mo', '--max-objects', type=int, default=np.inf, dest='max_objects', 
            help='Maximum number of submeshes generated (ordered by number of triangles).')
    mesh_args.add_argument('-mt', '--min-triangles', type=int, default=3, dest='min_triangles', 
            help='Minimum number of triangles that will be taken into account to create a submesh (defaults to 3).')
    mesh_args.add_argument('-mtp', '--min-triangle-percentage', type=float, default=0.01, dest='min_triangles_percentage', 
            help='Minimum number of triangles that will be taken into account to create a submesh in terms of percentage of original mesh triangles (defaults to 1 percent).')
    
    args = parser.parse_args()

    stl = args.input_file
    if (len(stl)<4) or (stl[-4:]!='.stl'):
        raise ValueError('Input file \'{}\' does not match input file format \'*.stl\'.'.format(stl))
    if not os.path.isfile(stl):
        raise ValueError('Input file \'{}\' does not exist.'.format(stl))

    if (args.output_basename is None):
        args.output_basename = stl.replace('.', '_{}.')
    if (args.output_basename.find('{}')<0):
        args.output_basename += '_{}'
    if (len(args.output_basename)<4) or (args.output_basename[-4:]!='.stl'):
        args.output_basename += '.stl'
    
    args.input_mesh_name  = args.input_file.split('/')[-1]
    args.output_mesh_name = args.output_basename.split('/')[-1]

    split_mesh(args)
